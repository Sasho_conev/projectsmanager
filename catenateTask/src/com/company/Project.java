package com.company;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Project {
    private String key;
    private String title;
    private Map<String,User> assignedUsers=new HashMap<>();
    private ArrayList <User> assignedUsersList=new ArrayList<>();

    public Project(String key, String title) {
        this.key = key;
        this.title = title;
    }



    public ArrayList<User> getAssignedUsersList() {
        return assignedUsersList;
    }

    public Map<String, User> getAssignedUsers() {
        return assignedUsers;
    }

    public void assignUser(String username, User user){
        assignedUsers.put(username,user);
        assignedUsersList.add(user);
    }

    public void removeUser(String username){
        assignedUsers.remove(username);
        for(int i=0;i<assignedUsersList.size();i++){
            if(assignedUsersList.get(i).getUsername().equals(username)){
                assignedUsersList.remove(i);
                break;
            }
        }
    }


    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
