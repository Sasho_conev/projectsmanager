package com.company;

import java.util.Scanner;

import static com.company.Main.*;
import static com.company.MainMenu.menu;

public class ManageProjectsMenu {

    public static void manageProjectsMenu(){
        Scanner scanner=new Scanner(System.in);
        char choice;

        System.out.println("Project management. Please select:");
        System.out.println("0-return to previous menu");
        System.out.println("1-create project");
        System.out.println("2-edit project");
        System.out.println("3-delete project");
        System.out.println("4-assign users to project");
        System.out.println("5-unassign user from a project");
        System.out.println("Enter your choice:");
        choice=scanner.nextLine().charAt(0);


        if(choice=='0'){
            System.out.println(newMenuSeparator);
            menu();
        }else if(choice=='1'){
            createProject();
        }else if(choice=='2'){
            editProjectMenu();;
        }else if(choice=='3'){
            deleteProject();
        }else if(choice=='4'){
            assignUserToProject();
        } else if(choice=='5'){
            unassignUserFromProject();
        } else {
            System.out.println("Invalid choice");
            System.out.println(newMenuSeparator);
            manageProjectsMenu();
        }

    }

    public static void createProject(){
        Scanner scanner=new Scanner(System.in);
        String key;
        String title;

        System.out.println("Project management. Creating new project:");
        System.out.println("Enter key:");
        key=scanner.nextLine();
        System.out.println("Enter title:");
        title= scanner.nextLine();

        Project newProject=new Project(key,title);
        projectsList.add(newProject);

        System.out.println(newMenuSeparator);
        System.out.println(String.format("Project %s created successfully",newProject.getKey()));
        manageProjectsMenu();
    }

    public static void editProjectMenu(){
        Scanner scanner=new Scanner(System.in);
        int choice;
        String newKey;
        String newTitle;

        System.out.println("Project management.Edit project");
        System.out.println("0-return to previous menu");
        for(int i=0;i< projectsList.size();i++){
            System.out.println(String.format("%d %s - %s",i+1,projectsList.get(i).getKey(),projectsList.get(i).getTitle()));
        }
        System.out.println();

        System.out.println("Please select:");
        choice=Integer.parseInt(scanner.nextLine());

        if(choice==0) {
            menu();
        } else if (choice > projectsList.size()) {
            System.out.println("Your choice is invalid!");
            editProjectMenu();
        }else{
            System.out.println("Use '-' for not changing the field");

            System.out.println("Enter new key:");
            newKey= scanner.nextLine();
            System.out.println("Enter new title:");
            newTitle= scanner.nextLine();

            if(!newKey.equals("-")) {
                projectsList.get(choice - 1).setKey(newKey);
            }
            if(!newTitle.equals("-")) {
                projectsList.get(choice - 1).setTitle(newTitle);
            }

            System.out.println(newMenuSeparator);
            System.out.println(String.format("Project '%s' edited successfully",newKey));
            menu();
        }
    }

    public static void deleteProject(){
        Scanner scanner=new Scanner(System.in);
        int choice;

        System.out.println("Project management.Delete project");
        System.out.println("0-return to previous menu");
        for(int i=0;i< projectsList.size();i++){
            System.out.println(String.format("%d %s - %s",i+1,projectsList.get(i).getKey(),projectsList.get(i).getTitle()));
        }
        System.out.println();

        System.out.println("Please select:");
        choice=Integer.parseInt(scanner.nextLine());

        if(choice==0) {
            menu();
        } else if (choice > projectsList.size()) {
            System.out.println(newMenuSeparator);
            System.out.println("Your choice is invalid!");
            editProjectMenu();
        }else{
            String deleteKey=projectsList.get(choice-1).getKey();
            projectsList.remove(choice-1);

            System.out.println(newMenuSeparator);
            System.out.println(String.format("Project '%s' deleted successfully",deleteKey));
            menu();
        }
    }

    public static void assignUserToProject() {
        Scanner scanner = new Scanner(System.in);
        int choice;

        System.out.println("Project management.Assign users to project:");
        System.out.println("0-return to previous menu");
        for (int i = 0; i < projectsList.size(); i++) {
            System.out.println(String.format("%d %s - %s", i + 1, projectsList.get(i).getKey(), projectsList.get(i).getTitle()));
        }
        System.out.println("Please select:");
        choice = Integer.parseInt(scanner.nextLine());

        if (choice == 0) {
            menu();
        } else if (choice > projectsList.size()) {
            System.out.println(newMenuSeparator);
            System.out.println("Your choice is invalid!");
            manageProjectsMenu();
        } else {
            Project assignedProject = projectsList.get(choice - 1);
            int userChoice;

            System.out.println("0-return to previous menu");
            for (int i = 0; i < usersList.size(); i++) {
                System.out.println(String.format("%d %s - %c",i+1, usersList.get(i).getUsername(), usersList.get(i).getRole()));
            }
            System.out.println("Please select");
            userChoice = Integer.parseInt(scanner.nextLine());

            if (userChoice == 0) {
                System.out.println(newMenuSeparator);
                assignUserToProject();
            } else if (userChoice > usersList.size()) {
                System.out.println(newMenuSeparator);
                System.out.println("Your choice is invalid!");
                manageProjectsMenu();
            } else {
                User user = usersList.get(userChoice - 1);
                if(assignedProject.getAssignedUsers().containsKey(user.getUsername())){
                    System.out.println("This user is already assigned to this project!");
                    manageProjectsMenu();
                }
                assignedProject.assignUser(user.getUsername(),user);
                user.addProject();
                System.out.println(newMenuSeparator);
                System.out.println(String.format("User %s assigned to project %s",user.getUsername(),assignedProject.getKey()));
                menu();

            }
        }
    }



    public static void unassignUserFromProject(){
        Scanner scanner = new Scanner(System.in);
        int choice;

        System.out.println("Project management.Unassign users from a project:");
        System.out.println("0-return to previous menu");
        for (int i = 0; i < projectsList.size(); i++) {
            System.out.println(String.format("%d %s - %s", i + 1, projectsList.get(i).getKey(), projectsList.get(i).getTitle()));
        }
        System.out.println("Please select:");
        choice = Integer.parseInt(scanner.nextLine());

        if (choice == 0) {
            menu();
        } else if (choice > projectsList.size()) {
            System.out.println(newMenuSeparator);
            System.out.println("Your choice is invalid!");
            manageProjectsMenu();
        }else{
            Project assignedProject = projectsList.get(choice - 1);
            int userChoice;

            System.out.println("0-return to previous menu");
            for (int i = 0; i < assignedProject.getAssignedUsersList().size(); i++) {
                System.out.println(String.format("%d %s - %c",i+1, assignedProject.getAssignedUsersList().get(i).getUsername(),assignedProject.getAssignedUsersList().get(i).getRole()));
            }
            System.out.println("Please select");
            userChoice = Integer.parseInt(scanner.nextLine());


            if (userChoice == 0) {
                assignUserToProject();
            } else if (userChoice > assignedProject.getAssignedUsersList().size()) {
                System.out.println(newMenuSeparator);
                System.out.println("Your choice is invalid!");
                manageProjectsMenu();
            }else {
                User user = assignedProject.getAssignedUsersList().get(choice - 1);
                assignedProject.removeUser(user.getUsername());
                user.removeProject();
                System.out.println(newMenuSeparator);
                System.out.println(String.format("User %s is unassigned from project %s",user.getUsername(),assignedProject.getKey()));
                manageProjectsMenu();
            }
        }

    }



}
