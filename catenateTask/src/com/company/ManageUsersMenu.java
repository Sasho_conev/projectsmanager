package com.company;

import java.sql.SQLOutput;
import java.util.Scanner;

import static com.company.Main.*;
import static com.company.MainMenu.menu;

public class ManageUsersMenu {

    public static void manageUsersMenu(){
        Scanner scanner=new Scanner(System.in);
        char choice;

        System.out.println("User management. Please select:");
        System.out.println("0-return to previous menu ");
        System.out.println("1-create user");
        System.out.println("2-edit user");
        System.out.println("3-delete user");
        System.out.println("Enter your choice:");
        choice=scanner.nextLine().charAt(0);

        if(choice == '0'){
            System.out.println(newMenuSeparator);
            menu();
        }else if(choice == '1'){
            createUser();
        }else if(choice == '2'){
            editUser();
        }else if(choice == '3'){
            deleteUser();
        }else{
            System.out.println(newMenuSeparator);
            System.out.println("invalid choice");
            manageUsersMenu();
        }
    }


    public static void createUser(){
        Scanner scanner=new Scanner(System.in);
        String username;
        String password;
        String fullName;
        char role;
        boolean existingUsername=false;

        System.out.println("User management. Creating new user:");

        do {
            System.out.println("Enter '-' for exit.");
            System.out.println("Enter username:");
            username = scanner.nextLine();
            if(username.equals("-")){
                System.out.println(newMenuSeparator);
                manageUsersMenu();
            }
            if(usersMap.containsKey(username)){
                System.out.println("This username already exists!Try again.");
                existingUsername=true;
            }
        }while(existingUsername==true);

        System.out.println("Enter password:");
        password=scanner.nextLine();
        if(password.equals("-")){
            System.out.println(newMenuSeparator);
            manageUsersMenu();
        }
        System.out.println("Enter full name:");
        fullName= scanner.nextLine();
        if(fullName.equals("-")){
            System.out.println(newMenuSeparator);
            manageUsersMenu();
        }
        System.out.println("Enter role A-admin,U-user,M-moderator");
        role=scanner.nextLine().charAt(0);
        Character.toLowerCase(role);
        if(role != 'a' && role != 'u' && role != 'm'){
            System.out.println(newMenuSeparator);
            System.out.println("The role you've entered is invalid!");
            menu();
        }
        else{
            User newUser=new User(username,password,fullName,role);
            usersMap.put(username,newUser);
            usersList.add(newUser);
            System.out.println(newMenuSeparator);
            System.out.println(String.format("User %s added successfully",newUser.getUsername()));
            menu();
        }
    }

    public static void editUser(){
        Scanner scanner=new Scanner(System.in);
        int choice;


        System.out.println("User management. Edit user:");
        System.out.println("0-return to previous menu");
        for(int i=0;i<usersList.size();i++){
            System.out.println(String.format("%d-%s",i+1,usersList.get(i).getUsername()));
        }

        System.out.println("Please select:");
        choice=Integer.parseInt(scanner.nextLine());




        if(choice==0){
            System.out.println(newMenuSeparator);
            menu();
        } else if (choice > usersList.size()) {
            System.out.println("Your choice is invalid!");
            editUser();
        }else{
            System.out.println("Use '-' for not changing the field");
            User editedUser=usersList.get(choice-1);

            String password;
            String fullName;
            char role;

            System.out.println("Enter password:");
            password=scanner.nextLine();
            System.out.println("Enter full name:");
            fullName= scanner.nextLine();
            if(choice!=1) {
                System.out.println("Enter role A-admin,U-user,M-moderator");
                role = scanner.nextLine().charAt(0);
                Character.toLowerCase(role);
            }else{
                role='a';//so you can not change the first admin's role
            }
            if(role != 'a' && role != 'u' && role != 'm' && role!='-'){
                System.out.println("The role you've entered is invalid!");
                editUser();
            }
            if(!password.equals("-")){
                editedUser.setPassword(password);
            }
            if(!fullName.equals("-")){
                editedUser.setFullName(fullName);
            }
            if(role!='-'){
                editedUser.setRole(role);
            }
            System.out.println(newMenuSeparator);
            System.out.println(String.format("User '%s' edited successfully",editedUser.getUsername()));
            menu();
        }
    }


    public static void deleteUser(){
        Scanner scanner=new Scanner(System.in);
        int choice;


        System.out.println("User management. Delete user:");
        System.out.println("0-return to previous menu");
        for(int i=0;i<usersList.size();i++){
            System.out.println(String.format("%d-%s",i+1,usersList.get(i).getUsername()));
        }

        System.out.println("Please select:");
        choice=Integer.parseInt(scanner.nextLine());

        if(choice==0){
            menu();
        }else if(choice==1){
            System.out.println(newMenuSeparator);
            System.out.println("You cant delete the first admin!");
            deleteUser();
        }
        else if (choice > usersList.size()) {
            System.out.println(newMenuSeparator);
            System.out.println("Your choice is invalid!");
            deleteUser();
        }else{
            String deleteUser=usersList.get(choice-1).getUsername();
            if(usersMap.get(deleteUser).getProjectsNumber()!=0){
                System.out.println(newMenuSeparator);
                System.out.println("You can not delete this user because he is assigned to a project!");
                menu();
            }
            usersList.remove(choice-1);
            usersMap.remove(deleteUser);
            System.out.println(newMenuSeparator);
            System.out.println(String.format("User '%s' deleted successfully",deleteUser));
            menu();
        }


    }


}
