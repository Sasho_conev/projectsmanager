package com.company;

import java.io.IOException;
import java.util.Scanner;


import static com.company.Main.*;
import static com.company.ManageProjectsMenu.manageProjectsMenu;
import static com.company.ManageUsersMenu.manageUsersMenu;

public class MainMenu {

    public static void menu(){

        Scanner scanner=new Scanner(System.in);
        char choice;

        if(currentUser == null) {
            System.out.println("0-Login");
        }else {
            System.out.println("0-Log out");
        }
        System.out.println("1-users management (Only for admins)");
        System.out.println("2-project management (Only for admins and moderators)");
        System.out.println("3-for exit");
        System.out.println("Enter your choice:");

        choice=scanner.next().charAt(0);

        if(choice=='0'){
            if(currentUser== null) {
                login();
            }else{
                logOut();
            }
        }else if(choice=='1'){
            if(currentUser == null){
                System.out.println("You are not logged in the system!");
                System.out.println(newMenuSeparator);
                menu();
            }
            else if (currentUser.getRole() != 'a'){
                System.out.println("You must be an admin to manage users!");
                System.out.println(newMenuSeparator);
                menu();
            }
            System.out.println(newMenuSeparator);
            manageUsersMenu();
        }else if(choice=='2'){
            if(currentUser == null){
                System.out.println("You are not logged in the system!");
                System.out.println(newMenuSeparator);
                menu();
            }
            else if (currentUser.getRole() != 'a' && currentUser.getRole()!= 'm'){
                System.out.println("You must be an admin or moderator to manage users!");
                System.out.println(newMenuSeparator);
                menu();
            }
            System.out.println(newMenuSeparator);
            manageProjectsMenu();
        }else if(choice=='3'){
            System.exit(0);
        }else {
            System.out.println(newMenuSeparator);
            System.out.println("Invalid choice");
            menu();

        }
    }





    public static void login(){
        Scanner scanner=new Scanner(System.in);
        String username;
        String password;

        System.out.println("Enter username:");
        username=scanner.nextLine();
        User loginUser= usersMap.get(username);
        if(loginUser == null){
            System.out.println(newMenuSeparator);
            System.out.println(String.format("User %s does not exist!",username));
            menu();
        }
        System.out.println("Enter password:");
        password=scanner.nextLine();
        if(!password.equals(loginUser.getPassword())){
            System.out.println(newMenuSeparator);
            System.out.println("Wrong password!");
            menu();
        }
        else{
            currentUser=usersMap.get(username);
            System.out.println(newMenuSeparator);
            System.out.println(String.format("You have logged in %s (%s)",currentUser.getUsername(),currentUser.getRole()));
            menu();
        }
    }

    public static void logOut(){
        currentUser = null;
        System.out.println(newMenuSeparator);
        menu();
    }

}
