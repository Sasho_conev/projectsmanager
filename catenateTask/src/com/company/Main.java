package com.company;

import java.lang.reflect.Array;
import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import static com.company.MainMenu.menu;

public class Main {


    static Map<String,User> usersMap=new HashMap();
    static ArrayList<User> usersList=new ArrayList<>();
    //the two containers are being used for different purposes

    static ArrayList<Project> projectsList=new ArrayList<>();

    static Map<User,ArrayList<Project>> userProjects=new HashMap<>();
    //So that we can not delete user that is assigned to a project

    static User currentUser;

    static String newMenuSeparator="--------------------";


    public static void main(String[] args) {
	   // write your code here
       User firstAdmin=new User("sasho","123","Sasho conev",'a');
       User another=new User("misho","123","Misho Angelov",'u');
       usersMap.put(firstAdmin.getUsername(),firstAdmin);
       usersMap.put(another.getUsername(),another);

       usersList.add(firstAdmin);
       usersList.add(another);


        menu();


    }
}
