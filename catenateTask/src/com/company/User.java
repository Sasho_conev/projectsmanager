package com.company;

public class User {
    private String username;
    private String password;
    private String fullName;
    private char role;
    private int projectsNumber;



    public User(String username, String password, String fullName, char role) {
        this.username = username;
        this.password = password;
        this.fullName = fullName;
        this.role = role;
        projectsNumber=0;
    }

    public int getProjectsNumber() {
        return projectsNumber;
    }

    public void addProject(){
        projectsNumber++;
    }

    public void removeProject(){
        projectsNumber--;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public char getRole() {
        return role;
    }

    public void setRole(char role) {
        this.role = role;
    }
}
